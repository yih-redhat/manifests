#!/usr/bin/env python3

import logging
import logging.config
import os
import sys
import json
from concurrent import futures
from pathlib import Path

import boto3
import requests
from jinja2 import Template
from requests.adapters import HTTPAdapter

import utils

def upload_directory(
    directory: str,
    prefix: str,
    bucket: str = os.environ["AWS_BUCKET_REPOS"],
    boto3_session: boto3.Session = boto3.Session(),
) -> int:
    ret = 0
    s3 = boto3_session.client("s3")

    with futures.ThreadPoolExecutor() as executor:
        upload_task = {}

        for filename in walk_directory(directory):
            upload_task[
                executor.submit(upload_file, filename, s3, bucket, prefix, directory)
            ] = filename

        for task in futures.as_completed(upload_task):
            try:
                task.result()
            except Exception as e:
                logging.error(
                    f"Exception {e} encountered while uploading file"
                    f"{upload_task[task]}"
                )
                ret = 1
    return ret


def error(e: Exception) -> None:
    logging.error(e)
    raise e


def walk_directory(directory: str) -> str:
    for root, _, files in os.walk(directory, onerror=error):
        for f in files:
            yield os.path.join(root, f)


def upload_file(
    filename: str, s3: boto3.client, bucket: str, prefix: str, directory: str
) -> None:
    s3.upload_file(
        Filename=filename,
        Bucket=bucket,
        Key=prefix + "/" + os.path.relpath(filename, directory),
        ExtraArgs={"ACL": "public-read"},
    )


def create_repo(repodir: str) -> int:
    return utils.run_cmd(cmd=["createrepo_c", repodir])


def test_repo(bucket: str, region: str, id: int) -> int:
    with open("/etc/yum.repos.d/automotive.repo", "w") as repofile:
        repofile.write(
            Template(
                open(
                    os.path.join(
                        (os.path.dirname(os.path.realpath(__file__))),
                        "yumrepo.j2",
                    ),
                ).read()
            ).render(bucket=bucket, region=region, id=id, centos_version=sys.argv[3]),
        )
    return utils.run_cmd(cmd=["dnf", "makecache", "--repo=auto-*"])


def create_directories(repodir: str, arch: str, repositories: list) -> None:
    for repo in repositories:
        Path(f"{repodir}/{repo}/{arch}/os").mkdir(parents=True)


def download_packages(
    repodir: str,
    supported_arch: str,
    repositories: list,
    mirror_list: list,
    centos_version: str,
) -> int:
    returnCode = 0
    s = requests.Session()
    s.mount("http://", HTTPAdapter(max_retries=5))

    file = open(
        f"{sys.argv[1]}/manifest_repo/package_list/{centos_version}-image-manifest.json"
    )
    json_data = json.load(file)

    package_arches = {
        "x86_64": ["x86_64", "i686", "noarch"],
        "aarch64": ["aarch64", "noarch"]
    }
    arches = package_arches[supported_arch]
    packages = json_data[f"{centos_version}"]["arch"][supported_arch] + json_data[f"{centos_version}"]["common"]
    for p in packages:

        package_downloaded = False
        for url in mirror_list:
            for repo in repositories:
                for arch in arches:
                    pkg = f"{p.strip()}.{arch}.rpm"
                    r = s.get(
                        url=(f"{url}{repo}/" f"{supported_arch}/os/Packages/{pkg}"),
                        allow_redirects=True,
                    )
                    if r.status_code == 200:
                        # workaround: S3 doesn't like the character '+' at the URL (PATH or file names)
                        # It's a known issue:
                        # https://stackoverflow.com/questions/36734171/how-to-decide-if-the-filename-has-a-plus-sign-in-it
                        # The next line will rename the files with '+' with '-'. This is done before the
                        # 'createrepo_c' indexes the packages, so the repo's metadata will point
                        # to the files with the new names. dnf/yum and the osbuild can now download
                        # and install the packages.
                        pkg_file = pkg.replace("+", "-")
                        with open(
                            f"{repodir}/{repo}/{supported_arch}/os/{pkg_file}", "wb"
                        ) as f:
                            f.write(r.content)
                        package_downloaded = True
                        logging.debug(f"{pkg} downloaded")
                        break  # pkg was found and was downloaded
                if package_downloaded:  # if pkg found in arches then break
                    break
            if package_downloaded:  # if pkg found in repos then break
                break
        if not package_downloaded:  # if pkg not found in any of the
            # arches*repos*mirrors then break
            logging.error(f"{p.strip()} was not found")
            file.close()
            returnCode = 1
            # break
    return returnCode


def main() -> int:
    log_config_path = Path(
        os.path.join(Path(__file__).parent.absolute(), "logging.conf")
    )

    logging.config.fileConfig(log_config_path)
    logging.getLogger("manifests")
    logging.info(":: create_yum_repo started ::")

    repodir = "/var/lib/repos"
    supported_arches = ["aarch64", "x86_64"]
    repositories = ["BaseOS", "AppStream", "extras"]

    centos_mirrors = {
        "cs8": [
            f"http://mirror.centos.org/centos/8-stream/",
            f"http://composes.centos.org/latest-CentOS-Stream-8/compose/",
        ],
        "cs9": [
            f"http://mirror.stream.centos.org/9-stream/",
            f"https://composes.stream.centos.org/development/latest-CentOS-Stream/",
        ],
    }

    centos_versions = sys.argv[3]
    pipeline_run_id = sys.argv[2]

    if type(centos_versions) == str:
        centos_versions = [sys.argv[3]]

    for version in centos_versions:
        for arch in supported_arches:
            if version not in centos_mirrors:
                logging.error(
                    f"unsupported/invalid value provided as parameter CENTOS_VERSION: {version}"
                )
                return 1
            else:
                create_directories(repodir, arch, repositories)
                returnCode = download_packages(
                    repodir, arch, repositories, centos_mirrors[version], version
                )

                if returnCode:
                    return 1

                # Show the list of downloaded packages
                packages_downloaded = []
                for repo in repositories:
                    packages_downloaded += Path(f"{repodir}/{repo}/{arch}/os").glob(
                        "*.rpm"
                    )
                packages_downloaded.sort()
                logging.info(f"Packages downloaded: {len(packages_downloaded)}")
                for pkg in packages_downloaded:
                    logging.info(pkg)

                for repo in repositories:
                    returnCode = create_repo(f"{repodir}/{repo}/{arch}/os")
                    if returnCode:
                        return 1

                returnCode = upload_directory(
                    directory=repodir,
                    prefix=f"{pipeline_run_id}/{version}",
                )
                if returnCode:
                    return 1

                returnCode = test_repo(
                    bucket=os.environ["AWS_BUCKET_REPOS"],
                    region=os.environ["AWS_REGION"],
                    id=pipeline_run_id,
                )
                if returnCode:
                    return 1

                with open("/etc/yum.repos.d/automotive.repo") as f:
                    logging.debug(f.read())
    return 0


if __name__ == "__main__":
    sys.exit(main())
