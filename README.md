# manifests

## CI

[CI documentation](https://gitlab.com/redhat/edge/ci-cd/manifests/-/blob/main/ci/README.md)

## Deployment

[Deployment documentation](https://gitlab.com/redhat/edge/ci-cd/manifests/-/blob/main/deployment/README.md)

## Vagrant

The local environment provided by Vagrant is based on CentOS Stream 9.

Supported providers:

* libvirt
* virtualbox

### Starting

```
# libvirt
vagrant up --provider=libvirt

# virtualbox
vagrant up --provider=virtualbox

# using default provider from an env var
export VAGRANT_DEFAULT_PROVIDER="libvirt"

vagrant up
```

All files of this repository will be synced in `/vagrant` inside the crated virtual machine.

### Destroying:

```
vagrant destroy
```
